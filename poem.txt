 Once upon a midnight dreary, while I pondered,
weak and weary,
Over many a quaint and curious volume of
forgotten 
Whether 'tis nobler in the mind to suffer
		The slings and arrows of outrageous fortune,    
No more; and by a sleep, to say we end
		The heart-ache, and the thousand natural shocks 
Or to take Arms against a Sea of troubles,
		And by opposing end them: to die, to sleep;     
  ,   .
      ...
     . 